
import java.io.*;
import java.util.Scanner;

public class Kotsys {

	public static void main(String args[]) throws IOException {
		// Scanner sc = new Scanner(System.in);
		// user input buffer reader instead of scanner
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		// available items
		String orderNameM[] = { " ", "Idly", "Dosa", "Pongal", "Idiyappam", "Rice and Curry", "Chinken curry",
				"Fish curry", "Mutton curry", "Egg curry", "Veg fridrice", "Egg fridrice", "Chicken fridrice",
				"Veg kothu", "Egg kothu", "Chicken kothu", "Veg noodels", "Egg noodles", "Chicken noodles", "Rotti",
				"Frutie juice", "Water bottel 1l", "Water bottel 500ml", "Coke2l", "Coke1l", "Coke750ml", "Coke300ml",
				"Pepsi2l", "Pepsi1l", "Pepsi750ml", "Pepsi300ml", "Sprite2l", "Sprite1l", "Sprite750ml", "Sprite300ml",
				"Necto2l", "Necto1l", "Necto750ml", "Necto300ml", "Weli Thalap", "Kalu dodol", "Aggala",
				"Avacado crazy", "Watlappam" };
		// available items with price
		String orderNameP[] = { " ", "Idly\tLKR 10.00", "Dosa\tLKR 20.00", "Pongal\tLKR 50.00", "Idiyappam\tLKR 8.00",
				"Rice and Curry\tLKR 150.00", "Chinken curry\tLKR 180.00", "Fish curry\tLKR 50.00",
				"Mutton curry\tLKR 250.00", "Egg curry\tLKR 50.00", "Veg fridrice\tLKR 130.00",
				"Egg fridrice\tLKR 150.00", "Chicken fridrice\tLKR 250.00", "Veg kothu\tLKR 150.00",
				"Egg kothu\tLKR 180.00", "Chicken kothu\tLKR 250.00", "Veg noodels\tLKR 130.00",
				"Egg noodles\tLKR 150.00", "Chicken noodles\tLKR 250.00", "Rotti\tLKR 20.00",
				"Frutie juice\tLKR 100.00", "Water bottel 1l\tLKR 70.00", "Water bottel 500ml\tLKR 35.00",
				"Coke2l\tLKR 350.00", "Coke1l\tLKR 200.00", "Coke750ml\tLKR 70.00", "Coke300ml\tLKR 50.00",
				"Pepsi2l\tLKR 350.00", "Pepsi1l\tLKR 200.00", "Pepsi750ml\tLKR 70.00", "Pepsi300ml\tLKR 50.00",
				"Sprite2l\tLKR 350.00", "Sprite1l\tLKR 200.00", "Sprite750ml\tLKR 70.00", "Sprite300ml\tLKR 50.00",
				"Necto2l\tLKR 350.00", "Necto1l\tLKR 200.00", "Necto750ml\tLKR 70.00", "Necto300ml\tLKR 50.00",
				"Weli Thalap\tLKR 100.00", "Kalu dodol\tLKR 100.00", "Aggala\tLKR 150.00", "Avacado crazy\tLKR 200.00",
				"Watlappam\tLKR 50.00" };
		// price
		double orderPrice[] = { 0.00, 10.00, 20.00, 50.00, 8.00, 150.00, 180.00, 50.00, 250.00, 50.00, 130.00, 150.00,
				250.00, 150.00, 180.00, 250.00, 130.00, 150.00, 250.00, 20.00, 100.00, 70.00, 35.00, 350.00, 200.00,
				70.00, 50.00, 350.00, 200.00, 70.00, 50.00, 350.00, 200.00, 70.00, 50.00, 350.00, 200.00, 70.00, 50.00,
				100.00, 100.00, 150.00, 200.00, 50 };
		String username, password, findout, again = "", mainQ = "", mainQ2 = "", dessertQ = "", dessertQ2 = "",
				drinksQ = "", drinksQ2 = "";

		int a1 = 0, b1 = 0, c1 = 0, d1 = 0, e1 = 1, g1 = 0, h1 = 0, i1 = 0, r1 = 0, select = 0, select1 = 0,
				placeOr = 0, orderNum = 1, close = 0;

		String order[][] = new String[50][20];
		String orderM[][] = new String[20][20];
		String orderP[][] = new String[20][20];
		double orderMD[][] = new double[50][20];
		double orderDD[][] = new double[50][20];
		double ordernum[] = new double[21];
		double pieces[] = new double[44];
		double cashed[] = new double[21];
		double allOrders[] = new double[50];
		double cash, balance = 0;
		double invoice[] = new double[21];
		double orderDouble[][] = new double[21][20];

		for (int z = 1; z <= 42; z++) {
			pieces[z] = 20;
		}

		while (a1 < 3) {
			System.out.println("-------------------------------------------------------"
					+ "\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>"
					+ "\n-------------------------------------------------------");
			System.out.print("\nUsername: ");
			username = in.readLine();
			System.out.print("\nPassword: ");
			password = in.readLine();

			if (username.equalsIgnoreCase("kot") && password.equalsIgnoreCase("Kot")) {

				System.out.println("\n-------------------------------------------------------"
						+ "\n|------------- Welcome to KOT Restaurant -------------|"
						+ "\n-------------------------------------------------------");

				do {
					System.out.println("\n-----------------------  K  O  T  ---------------------" + " \n(1) Order"
							+ " \n(2) Order Information" + " \n(3) Billing" + " \n(4) Dish Inventory" + " \n(5) Exit"
							+ "\n-------------------------------------------------------");

					for (int f = 1; f == 1;) {

						System.out.print("\nSelect: ");
						select = Integer.parseInt(in.readLine());

						// CHOICE 1 - "ORDER"

						if (select == 1) {
							do {
								for (int z = 1; z <= 4; z++) {
									order[b1][z] = "0";
								}
								for (int x = 1; x == 1;) {
									System.out.print("\nCustomer Name: ");
									order[b1][0] = in.readLine();

									x = 0;

									for (int l = 0; l < b1; l++) {
										if (order[l][0].equalsIgnoreCase(order[b1][0])) {
											// when you check the order details the same name will collapse
											System.out.println("Customer Name Already Used!!!"
													+ "\nPlease go with different name");
											x = 1;
										}
									}
								}

								c1 = 0;
								System.out.println("\n -------------------------------------"
										+ "\n --------- Available Dishes ----------"
										+ "\n -------------------------------------"
										+ "\n\n\n----------- Major Dishes ----------"
										+ "\n    -----------------------------------");

								// desserts which are available and the amount of desserts
								// ==========================================================
								System.out.print(" Idly\t\t\tLKR 10.00");
								if (pieces[1] > 0) {
									System.out.println("\t" + pieces[1] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Dosa\t\t\tLKR 20.00");
								if (pieces[2] > 0) {
									System.out.println("\t" + pieces[2] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Pongal\t\t\tLKR 50.00");
								if (pieces[3] > 0) {
									System.out.println("\t" + pieces[3] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Idiyappam\t\tLKR 8.00");
								if (pieces[4] > 0) {
									System.out.println("\t" + pieces[4] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Rice and Curry\t\tLKR 150.00");
								if (pieces[5] > 0) {
									System.out.println("\t" + pieces[5] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Chinken curry\t\tLKR 180.00");
								if (pieces[6] > 0) {
									System.out.println("\t" + pieces[6] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Mutton curry\t\tLKR 250.00");
								if (pieces[7] > 0) {
									System.out.println("\t" + pieces[7] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Egg curry\t\tLKR 50.00");
								if (pieces[8] > 0) {
									System.out.println("\t" + pieces[8] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Veg fridrice\t\tLKR 130.00");
								if (pieces[9] > 0) {
									System.out.println("\t" + pieces[9] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Egg fridrice\t\tLKR 150.00");
								if (pieces[10] > 0) {
									System.out.println("\t" + pieces[10] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Chicken fridrice\tLKR 250.00");
								if (pieces[11] > 0) {
									System.out.println("\t" + pieces[11] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Veg kothu\t\tLKR 150.00");
								if (pieces[12] > 0) {
									System.out.println("\t" + pieces[12] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Egg kothu\t\tLKR 180.00");
								if (pieces[13] > 0) {
									System.out.println("\t" + pieces[13] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Chicken kothu\t\tLKR 250.00");
								if (pieces[14] > 0) {
									System.out.println("\t" + pieces[14] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Veg noodels\t\tLKR 130.00");
								if (pieces[15] > 0) {
									System.out.println("\t" + pieces[15] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Egg noodles\t\tLKR 150.00");
								if (pieces[16] > 0) {
									System.out.println("\t" + pieces[16] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Chicken noodles\tLKR 250.00");
								if (pieces[17] > 0) {
									System.out.println("\t" + pieces[17] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print(" Rotti\t\t\tLKR 20.00");
								if (pieces[18] > 0) {
									System.out.println("\t" + pieces[18] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}

								// desserts which are available and the amount of desserts
								// ==========================================================
								System.out.println("\n    ------------- Dessert -------------"
										+ "\n    -----------------------------------");
								System.out.print("Weli Thalap\t\tLKR 100.00");
								if (pieces[19] > 0) {
									System.out.println("\t" + pieces[19] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Kalu dodol\t\tLKR 100.00");
								if (pieces[20] > 0) {
									System.out.println("\t" + pieces[20] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Aggala\t\t\tLKR 150.00");
								if (pieces[21] > 0) {
									System.out.println("\t" + pieces[21] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Avacado crazy\t\tLKR 200.00");
								if (pieces[22] > 0) {
									System.out.println("\t" + pieces[22] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Watlappam\t\tLKR 50.00");
								if (pieces[23] > 0) {
									System.out.println("\t" + pieces[23] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}

								// beverage which are available and the amount of beverage
								// ==========================================================
								System.out.println("\n    ------------- Beverages -------------"
										+ "\n    -----------------------------------");

								System.out.print("Frutie juice\t\tLKR 100.00");
								if (pieces[24] > 0) {
									System.out.println("\t" + pieces[24] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Water bottel 1l\t\tLKR 70.00");
								if (pieces[25] > 0) {
									System.out.println("\t" + pieces[25] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Water bottel 500ml\tLKR 35.00");
								if (pieces[26] > 0) {
									System.out.println("\t" + pieces[26] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Coke2l\t\t\tLKR 350.00");
								if (pieces[27] > 0) {
									System.out.println("\t" + pieces[27] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Coke1l\t\t\tLKR 200.00");
								if (pieces[28] > 0) {
									System.out.println("\t" + pieces[28] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Coke750ml\t\tLKR 70.00");
								if (pieces[29] > 0) {
									System.out.println("\t" + pieces[29] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Coke300ml\t\tLKR 50.00");
								if (pieces[30] > 0) {
									System.out.println("\t" + pieces[30] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Pepsi2l\t\t\tLKR 350.00");
								if (pieces[31] > 0) {
									System.out.println("\t" + pieces[31] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Pepsi1l\t\t\tLKR 200.00");
								if (pieces[32] > 0) {
									System.out.println("\t" + pieces[32] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Pepsi750ml\t\tLKR 70.00");
								if (pieces[33] > 0) {
									System.out.println("\t" + pieces[33] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Pepsi300ml\t\tLKR 50.00");
								if (pieces[34] > 0) {
									System.out.println("\t" + pieces[34] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Sprite2l\t\tLKR 350.00");
								if (pieces[35] > 0) {
									System.out.println("\t" + pieces[35] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Sprite1l\t\tLKR 200.00");
								if (pieces[36] > 0) {
									System.out.println("\t" + pieces[36] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Sprite750ml\t\tLKR 70.00");
								if (pieces[37] > 0) {
									System.out.println("\t" + pieces[37] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Sprite300ml\t\tLKR 50.00");
								if (pieces[38] > 0) {
									System.out.println("\t" + pieces[38] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Necto2l\t\t\tLKR 350.00");
								if (pieces[39] > 0) {
									System.out.println("\t" + pieces[39] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Necto1l\t\t\tLKR 200.00");
								if (pieces[40] > 0) {
									System.out.println("\t" + pieces[40] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Necto750ml\t\tLKR 70.00");
								if (pieces[41] > 0) {
									System.out.println("\t" + pieces[41] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}
								System.out.print("Necto300ml\t\tLKR 50.00");
								if (pieces[42] > 0) {
									System.out.println("\t" + pieces[42] + "pcs.");
								} else {
									System.out.println("\t*Not Available*");
								}

								System.out.println("\n    -------------------------------------------------------"
										+ "\n    -------------------------------------------------------");

								if (pieces[1] == 0 && pieces[2] == 0 && pieces[3] == 0 && pieces[4] == 0
										&& pieces[5] == 0 && pieces[6] == 0 && pieces[7] == 0 && pieces[8] == 0
										&& pieces[9] == 0 && pieces[10] == 0 && pieces[11] == 0 && pieces[12] == 0
										&& pieces[13] == 0 && pieces[14] == 0 && pieces[15] == 0 && pieces[16] == 0
										&& pieces[17] == 0 && pieces[18] == 0) {
									System.out.println("\nMajor dish Not Available!");
								} else {
									for (int v = 1; v == 1;) {
										System.out.print("\nDo you want to order Major Dishes? [Y/N]: ");
										mainQ = in.readLine();

										// Major dish

										if (mainQ.equalsIgnoreCase("y")) {

											do {
												System.out.println("\n\t ----------- Major Dishes -----------");
												System.out.println(" -------------------------------------");
												System.out.println(" NAME\t\t\t\tPRICE");

												System.out.print(" 1. Idly " + "\t\tLKR 10.00");
												if (pieces[1] > 0) {
													System.out.println("\t" + pieces[1] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 2. Dosa " + "\t\tLKR 20.00");
												if (pieces[2] > 0) {
													System.out.println("\t" + pieces[2] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 3. Pongal " + "\t\tLKR 50.00");
												if (pieces[3] > 0) {
													System.out.println("\t" + pieces[3] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 4. Idiyappam " + "\t\tLKR 8.00");
												if (pieces[4] > 0) {
													System.out.println("\t" + pieces[4] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 5. Rice and Curry " + "\tLKR 150.00");
												if (pieces[5] > 0) {
													System.out.println("\t" + pieces[5] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 6. Chinken curry " + "\tLKR 180.00");
												if (pieces[6] > 0) {
													System.out.println("\t" + pieces[6] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 7. Mutton curry " + "\tLKR 250.00");
												if (pieces[7] > 0) {
													System.out.println("\t" + pieces[7] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 8. Egg curry " + "\t\tLKR 50.00");
												if (pieces[8] > 0) {
													System.out.println("\t" + pieces[8] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 9. Veg fridrice " + "\tLKR 130.00");
												if (pieces[9] > 0) {
													System.out.println("\t" + pieces[9] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 10. Egg fridrice " + "\tLKR 150.00");
												if (pieces[10] > 0) {
													System.out.println("\t" + pieces[10] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 11. Chicken fridrice " + "\tLKR 250.00");
												if (pieces[11] > 0) {
													System.out.println("\t" + pieces[11] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 12. Veg kothu " + "\t\tLKR 150.00");
												if (pieces[12] > 0) {
													System.out.println("\t" + pieces[12] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 13. Egg kothu " + "\t\tLKR 180.00");
												if (pieces[13] > 0) {
													System.out.println("\t" + pieces[13] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 14. Chicken kothu " + "\tLKR 250.00");
												if (pieces[14] > 0) {
													System.out.println("\t" + pieces[14] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 15. Veg noodels " + "\tLKR 130.00");
												if (pieces[15] > 0) {
													System.out.println("\t" + pieces[15] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 16. Egg noodles " + "\tLKR 150.00");
												if (pieces[16] > 0) {
													System.out.println("\t" + pieces[16] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 17. Chicken noodles " + "\tLKR 250.00");
												if (pieces[17] > 0) {
													System.out.println("\t" + pieces[17] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 18. Rotti " + "\t\tLKR 20.00");
												if (pieces[18] > 0) {
													System.out.println("\t" + pieces[18] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.println(" .....................................");

												for (e1 = 1; e1 == 1;) {
													System.out.print("\nEnter Your Order: ");
													placeOr = Integer.parseInt(in.readLine());

													for (v = 1; v <= 18; v++) {
														if (placeOr == v) {
															if (order[b1][v].equals("1")) {
																System.out.println("Dish Already Listed!");
																e1 = 1;
															} else if (pieces[v] == 0) {
																System.out.println("Dish Not Available");
																e1 = 1;
															} else {
																e1 = 0;
															}
														}
													}
													if (placeOr < 1 || placeOr > 18) {
														System.out.println("Input Not Matched!");
														e1 = 1;
													}

												}

												do {
													System.out.print("How many? : ");
													orderMD[b1][c1] = Double.parseDouble(in.readLine());

													for (v = 1; v <= 4; v++) {
														if (placeOr == v) {
															order[b1][v] = "1";
															pieces[v] = pieces[v] - orderMD[b1][c1];
															if (pieces[v] < 0) {
																pieces[v] = pieces[v] + orderMD[b1][c1];
																System.out.println("Sorry, We only have " + pieces[v]
																		+ "pcs. Available");
																v = 5;
																r1 = 1;
															} else {
																r1 = 0;
															}
														}
													}
												} while (r1 == 1);

												orderP[b1][c1] = orderNameP[placeOr];
												orderM[b1][c1] = orderNameM[placeOr];
												orderDouble[b1][c1] = orderPrice[placeOr];

												c1++;

												for (d1 = 1; d1 == 1;) {
													System.out.print("\nWant to Order another Major Dishe? [Y/N]: ");
													mainQ2 = in.readLine();

													if (mainQ2.equalsIgnoreCase("y")) {
														d1 = 0;
													} else if (mainQ2.equalsIgnoreCase("n")) {
														System.out.print("");
														d1 = 0;
													} else {
														System.out.print("Input Not Matched!");
														d1 = 1;
													}
												}
												if (order[b1][1].equals("1") && order[b1][2].equals("1")
														&& order[b1][3].equals("1") && order[b1][4].equals("1")
														&& order[b1][5].equals("1") && order[b1][6].equals("1")
														&& order[b1][7].equals("1") && order[b1][8].equals("1")
														&& order[b1][9].equals("1") && order[b1][10].equals("1")
														&& order[b1][11].equals("1") && order[b1][12].equals("1")
														&& order[b1][13].equals("1") && order[b1][14].equals("1")
														&& order[b1][15].equals("1") && order[b1][16].equals("1")
														&& order[b1][17].equals("1") && order[b1][18].equals("1")) {
													System.out.println("\nSorry, You order all  Major dish!");
													mainQ2 = "n";
												}
												if (pieces[1] == 0 && pieces[2] == 0 && pieces[3] == 0 && pieces[4] == 0
														&& pieces[5] == 0 && pieces[6] == 0 && pieces[7] == 0
														&& pieces[8] == 0 && pieces[9] == 0 && pieces[10] == 0
														&& pieces[11] == 0 && pieces[12] == 0 && pieces[13] == 0
														&& pieces[14] == 0 && pieces[15] == 0 && pieces[16] == 0
														&& pieces[17] == 0 && pieces[18] == 0) {
													System.out.println("\nMajor dish Not Available!");
													mainQ2 = "n";
												}
											} while (mainQ2.equalsIgnoreCase("y"));

										} else if (mainQ.equalsIgnoreCase("n")) {
											v = 0;
										} else {
											System.out.print("Input Not Matched!");
											v = 1;
										}
									}
								}
								if (pieces[19] == 0 && pieces[20] == 0 && pieces[21] == 0 && pieces[22] == 0
										&& pieces[23] == 0) {
									System.out.println("\nMajor dish Not Available!");
								} else {
									for (int v = 1; v == 1;) {
										System.out.print("\nDo you want to order Desserts? [Y/N]: ");
										dessertQ = in.readLine();

										// DESSERT

										if (dessertQ.equalsIgnoreCase("y")) {
											do {
												System.out.println("\n\t ............ Desserts ............");
												System.out.println(" ----------------------------------");
												System.out.println(" NAME\t\t\t\tPRICE");
												System.out.print(" 1. Weli Thalap \t\tLKR 100.00");
												if (pieces[19] > 0) {
													System.out.println("\t" + pieces[19] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 2. Kalu dodol \t\tLKR 100.00");
												if (pieces[20] > 0) {
													System.out.println("\t" + pieces[20] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 3. Aggala \t\tLKR 150.00");
												if (pieces[21] > 0) {
													System.out.println("\t" + pieces[21] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 4. Avacado crazy \tLKR 200.00");
												if (pieces[22] > 0) {
													System.out.println("\t" + pieces[22] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print(" 5. Watlappam \tLKR 50.00");
												if (pieces[23] > 0) {
													System.out.println("\t" + pieces[23] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.println(" ----------------------------------");

												for (e1 = 1; e1 == 1;) {
													System.out.print("\nEnter Your Order: ");
													placeOr = Integer.parseInt(in.readLine());
													placeOr = placeOr + 4;

													for (v = 19; v <= 23; v++) {
														if (placeOr == v) {
															if (order[b1][v].equals("1")) {
																System.out.println("Dessert Already Listed!");
																e1 = 1;
															} else if (pieces[v] == 0) {
																System.out.println("Dessert Not Available");
																e1 = 1;
															} else {
																e1 = 0;
															}
														}
													}
													if (placeOr < 19 || placeOr > 23) {
														System.out.println("Input Not Matched!");
														e1 = 1;
													}
												}

												do {
													System.out.print("How many? : ");
													orderMD[b1][c1] = Double.parseDouble(in.readLine());

													for (v = 5; v <= 8; v++) {
														if (placeOr == v) {
															order[b1][v] = "1";
															pieces[v] = pieces[v] - orderMD[b1][c1];
															if (pieces[v] < 0) {
																pieces[v] = pieces[v] + orderMD[b1][c1];
																System.out.println("Sorry, We only have " + pieces[v]
																		+ "pcs. Available");
																v = 9;
																r1 = 1;

															} else {
																r1 = 0;
															}
														}

													}
												} while (r1 == 1);

												orderP[b1][c1] = orderNameP[placeOr];
												orderM[b1][c1] = orderNameM[placeOr];
												orderDouble[b1][c1] = orderPrice[placeOr];

												c1++;

												for (d1 = 1; d1 == 1;) {
													System.out.print("\nWant to Order Other DESSERT? [Y/N]: ");
													dessertQ2 = in.readLine();

													if (dessertQ2.equalsIgnoreCase("y")) {
														d1 = 0;
													} else if (dessertQ2.equalsIgnoreCase("n")) {
														d1 = 0;
														dessertQ = "n";
													} else {
														System.out.print("Input Not Matched!");
														d1 = 1;
													}
												}
												if (order[b1][19].equals("1") && order[b1][20].equals("1")
														&& order[b1][21].equals("1") && order[b1][22].equals("1")&& order[b1][23].equals("1")) {
													System.out.print("Sorry, You Order All DESSERT!");
													dessertQ2 = "n";
												}
												if (pieces[19] == 0 && pieces[20] == 0 && pieces[21] == 0
														&& pieces[22] == 0 && pieces[23] == 0) {
													System.out.println("\nMajor dish Not Available!");
													dessertQ2 = "n";
												}
											} while (dessertQ2.equalsIgnoreCase("y"));

										} else if (dessertQ.equalsIgnoreCase("n")) {
											v = 0;
										} else {
											System.out.print("Input Not Matched!");
											v = 1;
										}
									}
								}
								if (pieces[5] == 0 && pieces[6] == 0 && pieces[7] == 0 && pieces[8] == 0) {
									System.out.println("\nDessert Not Available!");
								} else {
									for (int v = 1; v == 1;) {
										System.out.print("\nDo you want to Order Beverages? [Y/N]: ");
										drinksQ = in.readLine();

										// DRINKS

										if (drinksQ.equalsIgnoreCase("y")) {

											do {
												System.out.println("\n\t ------------ Beverages------------");
												System.out.println(" ----------------------------------");
												System.out.println(" NAME\t\t\t\tPRICE");
												// ******************************************
												System.out.print("1. Frutie juice\t\tLKR 100.00");
												if (pieces[24] > 0) {
													System.out.println("\t" + pieces[24] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("2. Water bottel 1l\tLKR 70.00");
												if (pieces[25] > 0) {
													System.out.println("\t" + pieces[25] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("3. Water bottel 500ml\tLKR 35.00");
												if (pieces[26] > 0) {
													System.out.println("\t" + pieces[26] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("4. Coke2l\t\tLKR 350.00");
												if (pieces[27] > 0) {
													System.out.println("\t" + pieces[27] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("Coke1l\t\t\tLKR 200.00");
												if (pieces[28] > 0) {
													System.out.println("\t" + pieces[28] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("5. Coke750ml\t\tLKR 70.00");
												if (pieces[29] > 0) {
													System.out.println("\t" + pieces[29] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("6. Coke300ml\t\tLKR 50.00");
												if (pieces[30] > 0) {
													System.out.println("\t" + pieces[30] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("7. Pepsi2l\t\tLKR 350.00");
												if (pieces[31] > 0) {
													System.out.println("\t" + pieces[31] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("8. Pepsi1l\t\tLKR 200.00");
												if (pieces[32] > 0) {
													System.out.println("\t" + pieces[32] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("9. Pepsi750ml\t\tLKR 70.00");
												if (pieces[33] > 0) {
													System.out.println("\t" + pieces[33] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("10. Pepsi300ml\t\tLKR 50.00");
												if (pieces[34] > 0) {
													System.out.println("\t" + pieces[34] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("11. Sprite2l\t\tLKR 350.00");
												if (pieces[35] > 0) {
													System.out.println("\t" + pieces[35] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("12. Sprite1l\t\tLKR 200.00");
												if (pieces[36] > 0) {
													System.out.println("\t" + pieces[36] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("13. Sprite750ml\t\tLKR 70.00");
												if (pieces[37] > 0) {
													System.out.println("\t" + pieces[37] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("14. Sprite300ml\t\tLKR 50.00");
												if (pieces[38] > 0) {
													System.out.println("\t" + pieces[38] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("15. Necto2l\t\tLKR 350.00");
												if (pieces[39] > 0) {
													System.out.println("\t" + pieces[39] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("16. Necto1l\t\tLKR 200.00");
												if (pieces[40] > 0) {
													System.out.println("\t" + pieces[40] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("17. Necto750ml\t\tLKR 70.00");
												if (pieces[41] > 0) {
													System.out.println("\t" + pieces[41] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												System.out.print("18. Necto300ml\t\tLKR 50.00");
												if (pieces[42] > 0) {
													System.out.println("\t" + pieces[42] + "pcs.");
												} else {
													System.out.println("\t*Not Available*");
												}
												// ******************************************

												System.out.println(" ----------------------------------");

												for (e1 = 1; e1 == 1;) {
													System.out.print("\nEnter Your Order: ");
													placeOr = Integer.parseInt(in.readLine());
													placeOr = placeOr + 8;

													for (v = 24; v <= 42; v++) {
														if (placeOr == v) {
															if (order[b1][v].equals("1")) {
																System.out.println("Drinks Already Listed!");
																e1 = 1;
															} else if (pieces[v] == 0) {
																System.out.println("Drinks Not Available");
																e1 = 1;
															} else {
																e1 = 0;
															}
														}
													}
													if (placeOr < 24 || placeOr > 42) {
														System.out.println("Input Not Matched!");
														e1 = 1;
													}
												}

												orderP[b1][c1] = orderNameP[placeOr];
												orderM[b1][c1] = orderNameM[placeOr];
												orderDouble[b1][c1] = orderPrice[placeOr];

												do {

													System.out.print("How many? : ");
													orderMD[b1][c1] = Double.parseDouble(in.readLine());

													for (v = 24; v <= 42; v++) {
														if (placeOr == v) {
															order[b1][v] = "1";
															pieces[v] = pieces[v] - orderMD[b1][c1];
															if (pieces[v] < 0) {
																pieces[v] = pieces[v] + orderMD[b1][c1];
																System.out.println("Sorry, We only have " + pieces[v]
																		+ "pcs. Available");
																v = 13;
																r1 = 1;
															} else {
																r1 = 0;
															}
														}
													}
												} while (r1 == 1);

												c1++;

												for (d1 = 1; d1 == 1;) {
													System.out.print("\nWant to Order Other DRINKS? [Y/N]: ");
													drinksQ2 = in.readLine();

													if (drinksQ2.equalsIgnoreCase("y")) {
														d1 = 0;
														c1++;
													} else if (drinksQ2.equalsIgnoreCase("n")) {
														System.out.print("");
														d1 = 0;
														f = 0;
													} else {
														System.out.print("Input Not Matched!");
														d1 = 1;
													}
												}
												if (order[b1][9].equals("1") && order[b1][10].equals("1")
														&& order[b1][11].equals("1") && order[b1][12].equals("1")) {
													System.out.print("Sorry, You order all 4 DRINKS!");
													drinksQ2 = "n";
													f = 0;
												}
												if (pieces[24] == 0 && pieces[25] == 0 && pieces[26] == 0
														&& pieces[27] == 0 && pieces[28] == 0 && pieces[29] == 0
														&& pieces[30] == 0 && pieces[31] == 0 && pieces[32] == 0
														&& pieces[33] == 0 && pieces[34] == 0 && pieces[35] == 0
														&& pieces[36] == 0 && pieces[37] == 0 && pieces[38] == 0
														&& pieces[39] == 0 && pieces[40] == 0 && pieces[41] == 0
														&& pieces[42] == 0) {
													System.out.println("\nDrinks Not Available!");
													drinksQ2 = "n";
													f = 0;
												}
											} while (drinksQ2.equalsIgnoreCase("y"));

										} else if (drinksQ.equalsIgnoreCase("n")) {
											v = 0;
											f = 0;
										} else {
											System.out.print("Input Not Matched!");
											v = 1;
										}
									}
								}
								if (c1 == 0) {
									System.out.println("You Don't Have Any Order!");
									r1 = 1;
								} else {
									c1--;
								}
							} while (r1 == 1);
							ordernum[b1] = orderNum;
							allOrders[b1] = c1;
							cashed[b1] = 0;

							System.out.println("\nTable number is : " + ordernum[b1]);
							System.out.println(" Orders are : ");
							for (int y = 0; y <= allOrders[b1]; y++) {
								System.out.println("   " + orderMD[b1][y] + " pcs.\t" + orderM[b1][y]);
								invoice[b1] = invoice[b1] + (orderDouble[b1][y] * orderMD[b1][y]);
							}

							orderNum++;
							b1++;
						}

						// CHOICE 2 - "ORDER INFO"

						else if (select == 2) {

							do {

								System.out.print("\nCustomer Name: ");
								findout = in.readLine();

								int s = 1;

								for (int x = 0; x < b1; x++) {

									if (findout.equalsIgnoreCase(order[x][0])) {
										System.out.println(" -------  CUSTOMER ORDER INFO   -------");
										System.out.println(" Customer Name: " + order[x][0]);
										System.out.println(" Table Number: " + ordernum[x]);
										System.out.println(" Customer Order:");

										for (int y = 0; y <= allOrders[x]; y++) {
											System.out.println("   " + orderMD[x][y] + " Pcs\t" + orderP[x][y]);
										}
										System.out.println(" ----------------------------------");
										System.out.print(" Total Bill: LKR" + invoice[x]);
										if (cashed[x] == 1) {
											System.out.println(" *PAID*");
											System.out.println(" ----------------------------------");
										} else {
											System.out.println(" *NOT PAID*");
											System.out.println(" ----------------------------------");
											System.out.println("\n Please Proceed To Billing!");
										}
										s = 0;
									}

								}

								if (s == 1) {
									System.out.println("Customer Name not found!");
									g1 = 1;
								} else {
									g1 = 0;
								}

								f = 0;
							} while (g1 == 1);

						}

						else if (select == 3) {

							do {

								System.out.print("\nEnter Customer Name: ");
								findout = in.readLine();

								int s = 1;
								for (int x = 0; x < b1; x++) {
									if (findout.equalsIgnoreCase(order[x][0])) {
										System.out.println(" ----   CUSTOMER ORDER RECEIPT   ----");
										System.out.println(" Customer Name: " + order[x][0]);
										System.out.println(" Table Number: " + ordernum[x]);
										System.out.println(" Customer Order:");

										for (int y = 0; y <= allOrders[x]; y++) {
											System.out.println("   " + orderMD[x][y] + " Pcs\t" + orderP[x][y]);
										}
										System.out.println(" ----------------------------------");
										System.out.print(" Total Bill: LKR " + invoice[x]);
										if (cashed[x] == 1) {
											System.out.println(" *PAID*");
											System.out.println(" ----------------------------------");
										} else {
											System.out.println("\n ----------------------------------");
											for (int m = 1; m == 1;) {
												System.out.print("Enter Payment: ");
												cash = Double.parseDouble(in.readLine());

												balance = cash - invoice[x];
												if (balance < 0) {
													m = 1;
													System.out
															.println("Insufficient Amount of Money! Please Try Again!");
												} else {
													cashed[x] = 1;
													m = 0;
												}
											}
											System.out.println("Change: LKR" + balance);
										}

										s = 0;
									}
								}
								if (s == 1) {
									System.out.println("Customer Name not found!");
									h1 = 1;
								} else {
									h1 = 0;
								}
								f = 0;

							} while (h1 == 1);
						} else if (select == 4) {
							System.out.println(" ------------------- DISH  INVENTORY -------------------"
									+ "\n -------------------------------------------------------"
									+ "\n\n\n --------------------- Major Dishes --------------------"
									+ "\n -------------------------------------------------------\n");

							for (int z = 1; z <= 18; z++) {
								System.out.println(" \t" + pieces[z] + " pcs\t\t" + orderNameM[z]);
							}
							System.out.println("\n   ----------------------- Desserts ----------------------"
									+ "\n   -------------------------------------------------------\n");

							for (int z = 19; z <= 23; z++) {
								System.out.println(" \t" + pieces[z] + " pcs\t\t" + orderNameM[z]);
							}
							System.out.println("\n   ----------------------- Beverages ---------------------"
									+ "\n   -------------------------------------------------------\n");

							for (int z = 24; z <= 42; z++) {
								System.out.println(" \t" + pieces[z] + " pcs\t\t" + orderNameM[z]);
							}
							System.out.println("\n   -------------------------------------------------------");
							f = 0;
						} else if (select == 5) {
							f = 0;
							close = 1;
							again = "n";

							System.out.println(
									"\n\n     Have a good day" + "\n     Thanks for coming" + "\n     Come again...");

						} else {
							System.out.println("Input Not Matched !!!");
							f = 1;
						}

					}
					if (close == 0) {
						do {

							System.out.print("\nWish to go with another order [Y/N]?");
							again = in.readLine();

							if (again.equalsIgnoreCase("n")) {
								i1 = 0;
							} else if (again.equalsIgnoreCase("y")) {
								i1 = 0;
							} else {
								System.out.println("Input Not Matched !!!");
								i1 = 1;
							}
						} while (i1 == 1);
					}
				} while (again.equalsIgnoreCase("y"));

				break;
			} else {
				System.out.println("Check your Password or Username !!!");
				a1++;
			}
		}

	}

}