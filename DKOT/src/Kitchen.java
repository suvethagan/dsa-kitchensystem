
//It includes classes for inputs & outputs	
import java.io.*;
import java.util.Scanner;

//main class called Kitchen
public class Kitchen {
	// it throws certain methods to certain exception
	public static void main(String[] args) throws IOException {

		// one of the way to read the input from the keyboard
		Scanner sc = new Scanner(System.in);

		// the available menu items
		String MenuListItems[] = { " ", "Idly", "Dosa", "Pongal", "Idiyappam", "Rice and Curry", "Chinken curry",
				"Fish curry", "Mutton curry", "Egg curry", "Veg fridrice", "Egg fridrice", "Chicken fridrice",
				"Veg kothu", "Egg kothu", "Chicken kothu", "Veg noodels", "Egg noodles", "Chicken noodles", "Rotti",
				"Frutie juice", "Water bottel 1l", "Water bottel 500ml", "Coke2l", "Coke1l", "Coke750ml", "Coke300ml",
				"Pepsi2l", "Pepsi1l", "Pepsi750ml", "Pepsi300ml", "Sprite2l", "Sprite1l", "Sprite750ml", "Sprite300ml",
				"Necto2l", "Necto1l", "Necto750ml", "Necto300ml","Weli Thalap","Kalu dodol","Aggala", "Avacado crazy","Watlappam" };

		// the available item's price list
		String MenuListPrice[] = { " ", "Idly\tLKR 10.00", "Dosa\tLKR 20.00", "Pongal\tLKR 50.00",
				"Idiyappam\tLKR 8.00", "Rice and Curry\tLKR 150.00", "Chinken curry\tLKR 180.00",
				"Fish curry\tLKR 50.00", "Mutton curry\tLKR 250.00", "Egg curry\tLKR 50.00", "Veg fridrice\tLKR 130.00",
				"Egg fridrice\tLKR 150.00", "Chicken fridrice\tLKR 250.00", "Veg kothu\tLKR 150.00",
				"Egg kothu\tLKR 180.00", "Chicken kothu\tLKR 250.00", "Veg noodels\tLKR 130.00",
				"Egg noodles\tLKR 150.00", "Chicken noodles\tLKR 250.00", "Rotti\tLKR 20.00",
				"Frutie juice\tLKR 100.00", "Water bottel 1l\tLKR 70.00", "Water bottel 500ml\tLKR 35.00",
				"Coke2l\tLKR 350.00", "Coke1l\tLKR 200.00", "Coke750ml\tLKR 70.00", "Coke300ml\tLKR 50.00",
				"Pepsi2l\tLKR 350.00", "Pepsi1l\tLKR 200.00", "Pepsi750ml\tLKR 70.00", "Pepsi300ml\tLKR 50.00",
				"Sprite2l\tLKR 350.00", "Sprite1l\tLKR 200.00", "Sprite750ml\tLKR 70.00", "Sprite300ml\tLKR 50.00",
				"Necto2l\tLKR 350.00", "Necto1l\tLKR 200.00", "Necto750ml\tLKR 70.00", "Necto300ml\tLKR 50.00",
				"Weli Thalap\tLKR 100.00","Kalu dodol\tLKR 100.00","Aggala\tLKR 150.00", "Avacado crazy\tLKR 200.00",
				"Watlappam\tLKR 50.00" };

		// the price list of amount
		double MenuPrice[] = { 0.00, 10.00, 20.00, 50.00, 8.00, 150.00, 180.00, 50.00, 250.00, 50.00, 130.00, 150.00,
				250.00, 150.00, 180.00, 250.00, 130.00, 150.00, 250.00, 20.00, 100.00, 70.00, 35.00, 350.00, 200.00,
				70.00, 50.00, 350.00, 200.00, 70.00, 50.00, 350.00, 200.00, 70.00, 50.00, 350.00, 200.00, 70.00,
				50.00,100.00,100.00,150.00,200.00,50 };

		// to call the string list in the following functions declared here
		String username; 
		String password;
		String search;
		String again = ""; 
		String mainQ = ""; 
		String mainQ2 = ""; 
		String dessertQ = ""; 
		String dessertQ2 = "";
		String drinksQ = ""; 
		String drinksQ2 = "";

		// String details have to be added
		// ************************************************************************
		int[] pieces = null;

		// ************************************************************************
		for (int a1 = 1; a1 <= 12; a1++) {

			pieces[a1] = 40;
		}
		int b1 = 0;
		while (b1 < 3) {
			// user name password validation
			System.out.println("\nUser Name : ");
			username = sc.nextLine();
			System.out.println("\nPassword : ");
			password = sc.nextLine();
			// user name and password allocation, user name & password = "Rest"
			if (username.equalsIgnoreCase("Rest") && password.equalsIgnoreCase("Rest")) {
				// Restaurant name
				System.out.println("\n+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+="
						+ "\n+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=" + "\nWelcome to KOT Rest.In"
						+ "\n+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+="
						+ "\n+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=");
				do {
					System.out.println("\n+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+="
							+"\n+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+="
							+"Selection Portal"
							+"\n+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+="
							+"\n+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+="
							+"1. Available Item"
							+"2. Place Your Order"
							+"3. Ordered dishs Informations"
							+"4. Payment"
							+"5. Close"
							+"\n+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=");
//					first choice is Available item in the Restaurant 
					for(int f = 1; f==1;) {
						System.out.println("Place your selection : ");
						int choice = Integer.parseInt(sc.nextLine());
						
						if(choice ==1) {
							do {
								for(int z=1; z<=12;z++) {
									String[][] OrderString;
									int b;
									OrderString[b][z] = "0";
									for(int x=1; x ==1;) {
										System.out.println("Enter customer ID or Name");
										OrderString[b][0]=sc.nextLine();
										x = 0;
										for(int l = 0; l< b; l++) {
											if(OrderString[l][0].equalsIgnoreCase(OrderString[b][0])) {
												System.out.println("Name Or ID already used!!!");
												x=1;
											}
										}
									}
									c = 0;
									System.out.println("\n+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+="
											+"\n+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+="
											+"Dish Items"
											+"\n+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+="
											+"Food Items");
									System.out.print(" Adobong Baboy \t\tPhp 50.00");
									if (pieces[1] > 0) {
										System.out.println("\t" + pieces[1] + "pcs.");
									} else {
										System.out.println("\t*Not Available*");
									}
									System.out.print(" Adobong Manok \t\tPhp 50.00");
									if (pieces[2] > 0) {
										System.out.println("\t" + pieces[2] + "pcs.");
									} else {
										System.out.println("\t*Not Available*");
									}
									
									System.out.print(" Litchon baboy \t\tPhp 100.00");
									if (pieces[3] > 0) {
										System.out.println("\t" + pieces[3] + "pcs.");
									} else {
										System.out.println("\t*Not Available*");
									}
									System.out.print(" Litchon Manok \t\tPhp 150.00");
									if (pieces[4] > 0) {
										System.out.println("\t" + pieces[4] + "pcs.");
									} else {
										System.out.println("\t*Not Available*");
									}
									System.out.println("\n    *********** DESSERT ***********");
									System.out.print(" Ginataan \t\t\tPhp 25.00");
									if (pieces[5] > 0) {
										System.out.println("\t" + pieces[5] + "pcs.");
									} else {
										System.out.println("\t*Not Available*");
									}
									System.out.print(" CamoteCue \t\t\tPhp 20.00");
									if (pieces[6] > 0) {
										System.out.println("\t" + pieces[6] + "pcs.");
									} else {
										System.out.println("\t*Not Available*");
									}
									System.out.print(" Kalamay \t\t\tPhp 25.00");
									if (pieces[7] > 0) {
										System.out.println("\t" + pieces[7] + "pcs.");
									} else {
										System.out.println("\t*Not Available*");
									}
									System.out.print(" Nata de Coco \t\tPhp 20.00");
									if (pieces[8] > 0) {
										System.out.println("\t" + pieces[8] + "pcs.");
									} else {
										System.out.println("\t*Not Available*");
									}
									System.out.println("\n    *********** DRINKS ************");
									System.out.print(" Coke \t\t\t\tPhp 30.00");

								} 
							}
						}
					}
				}

			}
		}
	}
}
